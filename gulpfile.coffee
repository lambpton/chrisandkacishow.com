'use strict'

gulp = require 'gulp'
$ = require('gulp-load-plugins')({pattern:[
  'gulp-*'
  'del'
  'browser-sync'
  'wiredep'
]})

appDir = './app'
destDir = './dist'

wiredepConfig =
  exclude: [/bootstrap\/dist\/css\/.+/]
  fileTypes:
    html:
      replace:
        js: (filePath) -> "<script src='/scripts/vendor/#{filePath.split('/').pop()}'></script>"
        css: (filePath) -> "<link rel='stylesheet' href='/styles/vendor/#{filePath.split('/').pop()}'></link>"

gulp.task 'bower:js', (complete) ->
  if (jsFiles = $.wiredep(wiredepConfig).js)?
    gulp.src jsFiles
      .pipe(gulp.dest "#{destDir}/scripts/vendor")
  else
    complete()

gulp.task 'bower:css', (complete) ->
  if (cssFiles = $.wiredep(wiredepConfig).css)?
    gulp.src cssFiles
      .pipe(gulp.dest "#{destDir}/styles/vendor")
  else
    complete()

gulp.task 'bower', gulp.parallel('bower:js', 'bower:css')

gulp.task 'browser-sync', ->
  $.browserSync(
    server:
      baseDir: destDir
    port:9000
  )

gulp.task 'clean', (complete) ->
  $.del "#{destDir}/*", complete

gulp.task 'images', ->
  gulp.src "#{appDir}/images/**/*"
    .pipe(gulp.dest "#{destDir}/images")

gulp.task 'jade', ->
  gulp.src "#{appDir}/**/*.jade"
    .pipe($.jade({
      locals: require "#{appDir}/data/eats.json"
    }))
    .pipe(gulp.dest destDir)
    .pipe($.browserSync.reload(stream:true))

gulp.task 'less', ->
  gulp.src ["#{appDir}/styles/**/*.less", "!#{appDir}/styles/**/_*.less"]
    .pipe($.less({
      paths: ['./bower_components']
    }))
    .pipe(gulp.dest "#{destDir}/styles")
    .pipe($.browserSync.reload(stream:true))

gulp.task 'usemin', ->
  gulp.src "#{destDir}/index.html"
    .pipe($.usemin({
      css: [$.minifyCss(), 'concat', $.rev()]
      html: [$.minifyHtml({empty:true})]
      js: [$.uglify(), 'concat', $.rev()]
    }))
    .pipe(gulp.dest destDir)

gulp.task 'watch', ->
  gulp.watch "#{appDir}/index.jade", gulp.series('jade','wiredep')
  gulp.watch "#{appDir}/views/**/*.jade", ['jade']
  gulp.watch "#{appDir}/styles/**/*.less", ['less']

gulp.task 'wiredep', ->
  gulp.src "#{destDir}/index.html"
    .pipe($.wiredep.stream(wiredepConfig))
    .pipe(gulp.dest destDir)

# Dev server task
gulp.task('serve',
  gulp.series(
    'clean',
    gulp.parallel('jade', 'less', 'images'),
    'bower',
    'wiredep',
    gulp.parallel('browser-sync', 'watch')
  )
)

# Build task
gulp.task('default'
  gulp.series(
    'clean',
    gulp.parallel('jade', 'less', 'images'),
    'bower',
    'wiredep',
    'usemin'
  )
)
